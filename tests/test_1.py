"""
A test of the ci_testing project
"""


from starlette.testclient import TestClient

from main import app

client = TestClient(app)


def test_hello():
    """Checks the root uri of the server returns hello world"""
    response = client.get('/hello')
    assert response.status_code == 200
    assert response.json() == {"Hello": "World"}


def test_static_page():
    """Check the react static page"""
    response = client.get('/')
    assert response.status_code == 200
