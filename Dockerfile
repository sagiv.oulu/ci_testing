# Build client on node container
FROM node:8.16.1-alpine AS client
WORKDIR /app
COPY client/package*.json ./
RUN npm install
COPY client ./
RUN npm run build


FROM tiangolo/uvicorn-gunicorn-fastapi  
# :python3.7-alpine3.8
# Copy the client files built on the previos stage
COPY --from=client /app/build /var/www/html
WORKDIR /app
RUN pip install --upgrade pip
COPY requirements.txt ./
RUN pip install -r requirements.txt && \
  rm requirements.txt

COPY . /app