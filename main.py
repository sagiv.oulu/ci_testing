"""
A web application for testing the gitlab ci features
"""
from fastapi import FastAPI
from starlette.staticfiles import StaticFiles

app = FastAPI()


@app.get("/hello")
async def read_root() -> str:
    """
    A hello world api endpoint

    :return: a static response
    """
    return {"Hello": "World"}

app.mount("/",
          StaticFiles(directory="/var/www/html", html=True),
          name="static")
